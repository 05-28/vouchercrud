package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.entity.Product;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductRepository extends JpaRepository<Product,Integer>{
    
}
