package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.entity.Office;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IOfficeRepository extends JpaRepository<Office,Integer>{
    
}
