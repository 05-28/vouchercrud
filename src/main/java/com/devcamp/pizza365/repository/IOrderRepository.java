package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import com.devcamp.pizza365.model.COrder;

@Repository
public interface IOrderRepository extends JpaRepository<COrder, Long> {
	COrder findByOrderCode(String orderId);

	
}
