package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.entity.Employee;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IEmployeeRepository extends JpaRepository<Employee,Integer>{
    
}
