package com.devcamp.pizza365.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.devcamp.pizza365.entity.Payment;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface IPaymentRepository extends JpaRepository<Payment,Integer>{
    
    @Query(value = "SELECT * FROM payments WHERE check_number LIKE :checkNumber%",nativeQuery = true)
    List<Payment> findByCheckNumber(@Param("checkNumber") String checkNumber);

    @Query(value = "SELECT * FROM payments WHERE ammount LIKE :ammount%",nativeQuery = true)
    List<Payment> findByAmmount(@Param("ammount") BigDecimal ammount);



    //Chưa hoạt động được
    @Transactional
    @Modifying
    @Query(value = "UPDATE payments SET ammount = :ammount WHERE check_number = :checkNumber",nativeQuery = true)
    BigDecimal updateAmmount(@Param("ammount") BigDecimal ammount,@Param("checkNumber") String checkNumber);
    
}
