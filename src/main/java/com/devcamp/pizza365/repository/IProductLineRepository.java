package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.entity.ProductLine;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductLineRepository extends JpaRepository<ProductLine,Integer>{
    
}
