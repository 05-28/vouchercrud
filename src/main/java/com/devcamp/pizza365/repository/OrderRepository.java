package com.devcamp.pizza365.repository;

import java.util.List;

import com.devcamp.pizza365.entity.Order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface OrderRepository extends JpaRepository<Order,Integer>{
    @Query(value = "SELECT * FROM orders WHERE status LIKE :status%",nativeQuery = true)
	List<Order> findByStatus(@Param("status") String status);
}
