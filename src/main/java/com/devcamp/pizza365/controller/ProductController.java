package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.repository.IProductRepository;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProductController {
    
    @Autowired
    IProductRepository pProductRepository;

    @Autowired
    IProductLineRepository productLineRepository;
    //Lấy tất cả
    @GetMapping("/products")
    public ResponseEntity<List<Product>> getAllProducts(){
        try {
            List<Product> allProducts = new ArrayList<>();
            pProductRepository.findAll().forEach(allProducts::add);
            return new ResponseEntity<>(allProducts,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy theo id 
    @GetMapping("/products/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable("id") int id){
        try {
            Optional<Product> product = pProductRepository.findById(id);
            if(product.isPresent()){
                Product productData = product.get();
                return new ResponseEntity<>(productData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            } 
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá tất cả
    @DeleteMapping("/products")
    public ResponseEntity<Product> deleteAllProduct(){
        try {
            pProductRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá theo id
    @DeleteMapping("/products/{id}")
    public ResponseEntity<Product> deleteById(@PathVariable("id") int id){
        try {
            pProductRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo một product mới vào một product line theo id của product line
    @PostMapping("/productlines/{id}/products")
    public ResponseEntity<Object> createProduct(@PathVariable("id") int id,@RequestBody Product pProduct){
        try {
            Optional<ProductLine> findProductLine = productLineRepository.findById(id);
            if(findProductLine.isPresent()){
                ProductLine productLineData = findProductLine.get();
                Product newProduct = new Product();
                newProduct.setBuyPrice(pProduct.getBuyPrice());
                newProduct.setIdProductLine(productLineData.getId());
                newProduct.setOrderDetails(pProduct.getOrderDetails());
                newProduct.setProductCode(pProduct.getProductCode());
                newProduct.setProductDescripttion(pProduct.getProductDescripttion());
                newProduct.setProductName(pProduct.getProductName());
                newProduct.setProductScale(pProduct.getProductScale());
                newProduct.setProductVendor(pProduct.getProductVendor());
                newProduct.setQuantityInStock(pProduct.getQuantityInStock());
                newProduct.setProductLine(productLineData);

                Product saveProduct = pProductRepository.save(newProduct);
                return new ResponseEntity<>(saveProduct,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Cập nhật một product theo id
    @PutMapping("/products/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable("id") int id,@RequestBody Product pProduct){
        try {
            Optional<Product> findProduct = pProductRepository.findById(id);
            if(findProduct.isPresent()){
                Product productData = findProduct.get();
                productData.setBuyPrice(pProduct.getBuyPrice());
                productData.setOrderDetails(pProduct.getOrderDetails());
                productData.setProductCode(pProduct.getProductCode());
                productData.setProductDescripttion(pProduct.getProductDescripttion());
                productData.setProductName(pProduct.getProductName());
                productData.setProductScale(pProduct.getProductScale());
                productData.setProductVendor(pProduct.getProductVendor());
                productData.setQuantityInStock(pProduct.getQuantityInStock());

                Product saveProduct = pProductRepository.save(productData);
                return new ResponseEntity<>(saveProduct,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
