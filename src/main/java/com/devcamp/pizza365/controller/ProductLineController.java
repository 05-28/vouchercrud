package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class ProductLineController {
    
    @Autowired
    IProductLineRepository productLineRepository;
    //Lấy tất cả
    @GetMapping("/productlines")
    public ResponseEntity<List<ProductLine>> getAllProductLines(){
        try {
            List<ProductLine> allProductLines = new ArrayList<>();
            productLineRepository.findAll().forEach(allProductLines::add);
            return new ResponseEntity<>(allProductLines,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Lấy theo id 
    @GetMapping("/productlines/{id}")
    public ResponseEntity<ProductLine> getProductLineById(@PathVariable("id") int id){
        try {
            Optional<ProductLine> productline = productLineRepository.findById(id);
            if(productline.isPresent()){
                ProductLine productLineData = productline.get();
                return new ResponseEntity<>(productLineData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            } 
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá tất cả
    @DeleteMapping("/productlines")
    public ResponseEntity<ProductLine> deleteAllProductLine(){
        try {
            productLineRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Xoá theo id
    @DeleteMapping("/productlines/{id}")
    public ResponseEntity<ProductLine> deleteProductLineById(@PathVariable("id") int id){
        try {
            productLineRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Tạo một product mới vào một product line theo id của product line
    @PostMapping("/productlines")
    public ResponseEntity<Object> createProductLine(@PathVariable("id") int id,@RequestBody ProductLine pProductLine){
        try {
                ProductLine newProductLine = new ProductLine();
                newProductLine.setDescription(pProductLine.getDescription());
                newProductLine.setProductLine(pProductLine.getProductLine());
                newProductLine.setProducts(pProductLine.getProducts());

                ProductLine saveProductLine = productLineRepository.save(newProductLine);
                return new ResponseEntity<>(saveProductLine,HttpStatus.OK);
            } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Cập nhật một product theo id
    @PutMapping("/productlines/{id}")
    public ResponseEntity<Object> updateProductLine(@PathVariable("id") int id,@RequestBody ProductLine pProductLine){
        try {
            Optional<ProductLine> findProductLine = productLineRepository.findById(id);
            if(findProductLine.isPresent()){
                ProductLine productLineData = findProductLine.get();
                productLineData.setDescription(pProductLine.getDescription());
                productLineData.setProductLine(pProductLine.getProductLine());
                productLineData.setProducts(pProductLine.getProducts());

                ProductLine saveProductLine = productLineRepository.save(productLineData);
                return new ResponseEntity<>(saveProductLine,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
