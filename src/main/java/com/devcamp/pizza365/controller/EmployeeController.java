package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;
import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
@CrossOrigin
public class EmployeeController {
    
    @Autowired
    IEmployeeRepository employeeRepository;

   
    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllEmployees(){
        try {
            List<Employee> allEmployees = new ArrayList<>();
            employeeRepository.findAll().forEach(allEmployees::add);
            return new ResponseEntity<>(allEmployees,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") int id){
        try {
            Optional<Employee> employee = employeeRepository.findById(id);
            if(employee.isPresent()){
                Employee employeeData = employee.get();
                return new ResponseEntity<>(employeeData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            } 
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   
    @DeleteMapping("/employees")
    public ResponseEntity<Employee> deleteAllEmployee(){
        try {
            employeeRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/employees/{id}")
    public ResponseEntity<Employee> deleteEmployeeById(@PathVariable("id") int id){
        try {
            employeeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PostMapping("/employees")
    public ResponseEntity<Object> createEmployee(@PathVariable("id") int id,@RequestBody Employee pEmployee){
        try {
                Employee newEmployee = new Employee();
                newEmployee.setEmail(pEmployee.getEmail());
                newEmployee.setExtension(pEmployee.getExtension());
                newEmployee.setFirstName(pEmployee.getFirstName());
                newEmployee.setJobTitle(pEmployee.getJobTitle());
                newEmployee.setLastName(pEmployee.getLastName());
                newEmployee.setOfficeCode(pEmployee.getOfficeCode());
                newEmployee.setReportTo(pEmployee.getReportTo());

                Employee saveEmployee = employeeRepository.save(newEmployee);
                return new ResponseEntity<>(saveEmployee,HttpStatus.OK);
            } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/employees/{id}")
    public ResponseEntity<Object> updateEmployee(@PathVariable("id") int id,@RequestBody Employee pEmployee){
        try {
            Optional<Employee> findEmployee = employeeRepository.findById(id);
            if(findEmployee.isPresent()){
                Employee employeeData = findEmployee.get();
                employeeData.setEmail(pEmployee.getEmail());
                employeeData.setExtension(pEmployee.getExtension());
                employeeData.setFirstName(pEmployee.getFirstName());
                employeeData.setJobTitle(pEmployee.getJobTitle());
                employeeData.setLastName(pEmployee.getLastName());
                employeeData.setOfficeCode(pEmployee.getOfficeCode());
                employeeData.setReportTo(pEmployee.getReportTo());

                Employee saveEmployee = employeeRepository.save(employeeData);
                return new ResponseEntity<>(saveEmployee,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
