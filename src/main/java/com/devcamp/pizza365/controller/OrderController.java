package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;
import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
    
    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ICustomerRepository customerRepository;

	@GetMapping("/orders/status/{status}")
	public ResponseEntity<List<Order>> getOrderByStatus(@PathVariable("status") String status){
		try {
			List<Order> vOrder = new ArrayList<Order>();
			orderRepository.findByStatus(status).forEach(vOrder::add);
			return new ResponseEntity<>(vOrder,HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/orders")
    public ResponseEntity<List<Order>> getAllOrders() {
        try {
            List<Order> allOrders = new ArrayList<>();
            orderRepository.findAll().forEach(allOrders::add);
            return new ResponseEntity<>(allOrders, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders/{id}")
    public ResponseEntity<Order> getOrderById(@PathVariable("id") int id) {
        try {
            Optional<Order> order = orderRepository.findById(id);
            if (order.isPresent()) {
                Order orderData = order.get();
                return new ResponseEntity<>(orderData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/orders")
    public ResponseEntity<Order> deleteAllOrders() {
        try {
            orderRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/orders/{id}")
    public ResponseEntity<Order> deleteOrderById(@PathVariable("id") int id) {
        try {
            orderRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PostMapping("/customers/{id}/orders")
    public ResponseEntity<Object> createPayment(@PathVariable("id") int id,@RequestBody Order pOrder){
        try {
                Optional<Customer> findCustomer = customerRepository.findById(id);
                if(findCustomer.isPresent()){
                    Customer customerData = findCustomer.get();
                    Order newOrder = new Order();
                    newOrder.setComments(pOrder.getComments());
                    newOrder.setOrderDate(pOrder.getOrderDate());
                    newOrder.setOrderDetails(pOrder.getOrderDetails());
                    newOrder.setRequiredDate(pOrder.getRequiredDate());
                    newOrder.setShippedDate(pOrder.getShippedDate());
                    newOrder.setStatus(pOrder.getStatus());
                    newOrder.setCustomer(customerData);

                    Order saveOrder = orderRepository.save(newOrder);
                return new ResponseEntity<>(saveOrder,HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/orders/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable("id") int id, @RequestBody Order pOrder) {
        try {
            Optional<Order> findOrder = orderRepository.findById(id);
            if (findOrder.isPresent()) {
                Order orderData = findOrder.get();
                orderData.setComments(pOrder.getComments());
                orderData.setOrderDate(pOrder.getOrderDate());
                orderData.setOrderDetails(pOrder.getOrderDetails());
                orderData.setRequiredDate(pOrder.getRequiredDate());
                orderData.setShippedDate(pOrder.getShippedDate());
                orderData.setStatus(pOrder.getStatus());

                Order saveOrder = orderRepository.save(orderData);
                return new ResponseEntity<>(saveOrder, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
