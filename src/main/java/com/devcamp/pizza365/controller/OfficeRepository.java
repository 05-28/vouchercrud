package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
@RequestMapping
@CrossOrigin
public class OfficeRepository {
    
    @Autowired
    IOfficeRepository officeRepository;

    @GetMapping("/offices")
    public ResponseEntity<List<Office>> getAllOffices(){
        try {
            List<Office> allOffices = new ArrayList<>();
            officeRepository.findAll().forEach(allOffices::add);
            return new ResponseEntity<>(allOffices,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @GetMapping("/offices/{id}")
    public ResponseEntity<Office> getOfficeById(@PathVariable("id") int id){
        try {
            Optional<Office> office = officeRepository.findById(id);
            if(office.isPresent()){
                Office officeData = office.get();
                return new ResponseEntity<>(officeData,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            } 
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

   
    @DeleteMapping("/offices")
    public ResponseEntity<Office> deleteAllOffice(){
        try {
            officeRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/offices/{id}")
    public ResponseEntity<Office> deleteOfficeById(@PathVariable("id") int id){
        try {
            officeRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
    @PostMapping("/offices")
    public ResponseEntity<Object> createOffice(@PathVariable("id") int id,@RequestBody Office pOffice){
        try {
                Office newOffice = new Office();
                newOffice.setAddressLine(pOffice.getAddressLine());
                newOffice.setCity(pOffice.getCity());
                newOffice.setCountry(pOffice.getCountry());
                newOffice.setPhone(pOffice.getPhone());
                newOffice.setState(pOffice.getState());
                newOffice.setTerritory(pOffice.getTerritory());

                Office saveOffice = officeRepository.save(newOffice);
                return new ResponseEntity<>(saveOffice,HttpStatus.OK);
            } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @PutMapping("/offices/{id}")
    public ResponseEntity<Object> updateOffice(@PathVariable("id") int id,@RequestBody Office pOffice){
        try {
            Optional<Office> findOffice = officeRepository.findById(id);
            if(findOffice.isPresent()){
                Office officeData = findOffice.get();
                officeData.setAddressLine(pOffice.getAddressLine());
                officeData.setCity(pOffice.getCity());
                officeData.setCountry(pOffice.getCountry());
                officeData.setPhone(pOffice.getPhone());
                officeData.setState(pOffice.getState());
                officeData.setTerritory(pOffice.getTerritory());

                Office saveOffice = officeRepository.save(officeData);
                return new ResponseEntity<>(saveOffice,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
