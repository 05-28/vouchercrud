package com.devcamp.pizza365.controller;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IPaymentRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import org.apache.catalina.connector.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/")
public class PaymentController {

    @Autowired
    IPaymentRepository paymentRepository;

    @Autowired
    ICustomerRepository customerRepository;

    @GetMapping("/payments")
    public ResponseEntity<List<Payment>> getAllPayments() {
        try {
            List<Payment> allPayments = new ArrayList<>();
            paymentRepository.findAll().forEach(allPayments::add);
            return new ResponseEntity<>(allPayments, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/payments/ammount/{ammount}")
    public ResponseEntity<List<Payment>> getPaymentByAmmount(@PathVariable BigDecimal ammount){
        try {
            List<Payment> vPayment = new ArrayList<Payment>();
            paymentRepository.findByAmmount(ammount).forEach(vPayment::add);
            return new ResponseEntity<>(vPayment,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/payments/check-number/{checkNumber}")
    public ResponseEntity<List<Payment>> getPaymentByCheckNumber(@PathVariable String checkNumber){
        try {
            List<Payment> vPayment = new ArrayList<Payment>();
            paymentRepository.findByCheckNumber(checkNumber).forEach(vPayment::add);
            return new ResponseEntity<>(vPayment,HttpStatus.OK);
        } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/payments/{id}")
    public ResponseEntity<Payment> getPaymentById(@PathVariable("id") int id) {
        try {
            Optional<Payment> payment = paymentRepository.findById(id);
            if (payment.isPresent()) {
                Payment paymentData = payment.get();
                return new ResponseEntity<>(paymentData, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/payments")
    public ResponseEntity<Payment> deleteAllOffice() {
        try {
            paymentRepository.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/payments/{id}")
    public ResponseEntity<Payment> deleteOfficeById(@PathVariable("id") int id) {
        try {
            paymentRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customers/{id}/payments")
    public ResponseEntity<Object> createPayment(@PathVariable("id") int id,@RequestBody Payment pPayment){
        try {
                Optional<Customer> findCustomer = customerRepository.findById(id);
                if(findCustomer.isPresent()){
                    Customer customerData = findCustomer.get();
                    Payment newPayment = new Payment();
                    newPayment.setAmmount(pPayment.getAmmount());
                    newPayment.setCheckNumber(pPayment.getCheckNumber());
                    newPayment.setPaymentDate(pPayment.getPaymentDate());
                    newPayment.setCustomer(customerData);

                    Payment savePayment = paymentRepository.save(newPayment);
                return new ResponseEntity<>(savePayment,HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
            } catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/payments/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable("id") int id, @RequestBody Payment pPayment) {
        try {
            Optional<Payment> findPayment = paymentRepository.findById(id);
            if (findPayment.isPresent()) {
                Payment paymentdata = findPayment.get();
                paymentdata.setAmmount(pPayment.getAmmount());
                paymentdata.setCheckNumber(pPayment.getCheckNumber());
                paymentdata.setPaymentDate(pPayment.getPaymentDate());

                Payment savePayment = paymentRepository.save(paymentdata);
                return new ResponseEntity<>(savePayment, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
